---
title: "About"
description: "Life is short, or long, depending on how you look at it. The number of things a person can learn in a lifetime is tiny compared to the wholeness of everything that could be learned... but we value learning for its own sake, even though it can never be completed."
featured_image: ''
---
{{< figure src="/images/too_many_books.jpg" title="How can we possibly get through it all?!?" >}}

My focus at _Your Learning Life_ is to help you find the space, structures, and necessary support to make (lifelong) learning a priority. Learning is great for getting you ahead in the world, but for some of us, it's like breathing.

I've said a few times that I'm what happens if a two year old never learns to stop asking, "But why, mummy? WHY?"
