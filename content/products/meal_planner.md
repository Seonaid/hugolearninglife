---
date: 2020-03-03T10:58:08-04:00
description: ""
featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["planners"]
title: "Max-Flex Meal Planning"
---

{{< figure src="/images/too_many_books.jpg" title="What Does Meal Planning Have to do With Learning?" >}}

I'm glad you asked!

Here's the deal. We've got to eat. 

A huge amount of all our time is taken up with managing the necessities of life, and eating is a pretty basic necessity. One of my goals is to make it easier for people to meet their needs by getting good at things, which makes them easier, and therefore less time consuming. 

And thus, they have more time available for the things they really _want_ to do.

Make sense?

You also might like to learn... How to Meal Plan!

<a href="https://www.amazon.ca/dp/B085DQXFQH/">{{< figure src="/images/meal_planner.png" title="Canadian Link" >}}</a>

In this planner, I take a "start where you are" approach. I'll help you build running lists of what you've got on hand and what your favourite meals are, as well as encourage you to do a weekly check in on what needs to be used up before it goes bad.

This approach moves the hard part of decision making to your down time, reducing the weekly "planning" task to choosing from a list of options. Easy options. Options you selected in the first place.

Also, my daughter laughed in all the right places during the intro and instructions, so you should probably buy it and find out why.